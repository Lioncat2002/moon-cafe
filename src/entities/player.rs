use bevy::prelude::*;

#[derive(Component, Clone)]
pub struct Player;
pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, start);
        app.add_systems(Update, update);
    }
}

pub fn start() {}
pub fn update() {}
