use bevy::{prelude::*, render::camera::ScalingMode};
use bevy_rapier2d::{
    dynamics::{GravityScale, LockedAxes, RigidBody},
    geometry::Collider,
    plugin::{NoUserData, RapierPhysicsPlugin},
    render::RapierDebugRenderPlugin,
};
use entities::player::Player;
mod entities;

fn start(mut commands: Commands, asset_server: Res<AssetServer>) {
    let mut camera = Camera2dBundle::default();
    camera.projection.scaling_mode = ScalingMode::AutoMin {
        min_width: 256.0,
        min_height: 144.0,
    };
    commands.spawn(camera);
    let map = asset_server.load("map.png");
    commands.spawn(SpriteBundle {
        texture: map,
        ..Default::default()
    });
    commands.spawn((
        Collider::cuboid(40., 4.),
        TransformBundle::from(Transform::from_xyz(0., 36., 10.)),
    ));
    commands.spawn((
        Collider::cuboid(4., 24.),
        TransformBundle::from(Transform::from_xyz(-36., 8., 10.)),
    ));
    commands.spawn((
        Collider::cuboid(4., 24.),
        TransformBundle::from(Transform::from_xyz(36., 8., 10.)),
    ));
    commands.spawn((
        Collider::cuboid(40., 4.),
        TransformBundle::from(Transform::from_xyz(0., -20., 10.)),
    ));
    let player = asset_server.load("player.png");
    commands.spawn((
        SpriteBundle {
            texture: player,
            ..Default::default()
        },
        //RigidBody::Dynamic,
        Collider::cuboid(4., 4.),
        Player,
    ));
}

fn main() {
    App::new()
        .add_plugins(
            DefaultPlugins
                .set(ImagePlugin::default_nearest())
                .set(WindowPlugin {
                    primary_window: Some(Window {
                        title: "moon cafe".into(),
                        resolution: (853.3, 480.0).into(),
                        resizable: false,
                        ..Default::default()
                    }),
                    ..Default::default()
                })
                .build(),
        )
        .add_plugins(RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(32.0))
        .add_plugins(RapierDebugRenderPlugin::default())
        .add_plugins(entities::player::PlayerPlugin)
        .add_systems(Startup, start)
        .run();
}
